'use strict';

var mongoose = require('mongoose');

var messageSchema = {
  messsage: {
    type: String,
    required: true
  },
  from: {
    type: String,
    required: true
  },
  to: {
    type: String,
    required: true
  },
  conversationId: {
    type: String,
    required: true
  },
  time: {
    type: Date,
    default: Date.now
  },
  status: {
    type: Number,
    default: 1
  }
};

module.exports = {
  MessageSchema: new mongoose.Schema(messageSchema)
};