'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var socketEcho = require('../utils/sockets');
var Mongo = require('../utils/mongo');
var lib = require('../utils/misc-libraries');

var Chat = function () {
    function Chat(message) {
        _classCallCheck(this, Chat);

        this._message = message;
    }

    _createClass(Chat, [{
        key: 'sendMessage',
        value: function sendMessage() {}
    }, {
        key: '_checkValidMessage',
        value: function _checkValidMessage() {
            if (['message', 'from', 'to', 'conversationId', 'id'] in this._message) {
                // check if conversationId is valid
                // check if from is valid
                // check if to is valid
                return true;
            } else return false;
        }
    }, {
        key: '_serverPunchingMessage',
        value: function _serverPunchingMessage() {
            if (this._checkValidMessage) {
                _this.message.time = new Date();
                _this.message.status = 1;
            }
        }
    }, {
        key: '_notifyReceiver',
        value: function _notifyReceiver() {}
    }, {
        key: '_acknowledgeReceiver',
        value: function _acknowledgeReceiver() {}
    }]);

    return Chat;
}();

exports.default = Chat;