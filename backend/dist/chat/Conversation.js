'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var uuid = require('../utils/misc-libraries').generateUuid;
var MessageModel = require('./MessageModel');

var Conversation = function () {
    function Conversation() {
        var conversationId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        var users = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

        _classCallCheck(this, Conversation);

        if (conversationId) this._conversationId = conversationId;
    }

    _createClass(Conversation, [{
        key: 'generateConversationId',
        value: function generateConversationId(users, callback) {
            var _this = this;
            var _new = false;

            function respond(resp) {
                if (resp.length) {
                    _this._conversationId = resp[0].conversationId;
                } else {
                    _this._conversationId = uuid();
                    _new = true;
                }
                console.log(_this._conversationId);
                var response = {
                    conversationId: _this._conversationId,
                    new: _new
                };
                if (!_new) {
                    response.messages = resp;
                }
                callback(response);
            }
            this.getConversationUsers(users, respond);
        }
    }, {
        key: 'getConversationId',
        value: function getConversationId() {
            return this._conversationId;
        }
    }, {
        key: 'getLastMessage',
        value: function getLastMessage(callback) {
            this.getMessages(function (docs) {
                callback(docs[0] || null);
            }, 1, -1);
        }
    }, {
        key: 'getMessages',
        value: function getMessages(callBack) {
            var limit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var timeSort = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

            MessageModel.find({
                conversationId: this._conversationId
            }).sort({
                time: timeSort
            }).limit(limit).then(function (doc) {
                callBack(doc);
            }).catch(function (err) {
                console.error(err);
                // callBack(false);
            });
        }
    }, {
        key: 'getConversationUsers',
        value: function getConversationUsers(users, callBack) {
            var query = {
                '$or': [{
                    'from': users[0],
                    'to': users[1]
                }, {
                    'from': users[1],
                    'to': users[0]
                }]
            };
            console.log(query);
            MessageModel.find(query).then(function (doc) {
                console.log(doc, "SOMETHING");
                callBack(doc);
            }).catch(function (err) {
                console.error(err);
                callBack(false);
            });
        }
    }, {
        key: 'delete',
        value: function _delete() {}
    }, {
        key: 'archive',
        value: function archive() {}
    }]);

    return Conversation;
}();

exports.default = Conversation;