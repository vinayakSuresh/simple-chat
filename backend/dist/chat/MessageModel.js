'use strict';

var mongoose = require('mongoose');
var messageSchema = new mongoose.Schema({
  message: {
    type: String,
    required: true
  },
  from: {
    type: String,
    required: true
  },
  to: {
    type: String,
    required: true
  },
  conversationId: {
    type: String,
    required: true
  },
  time: {
    type: Date,
    default: Date.now
  },
  status: {
    type: Number,
    default: 1
  },
  id: {
    type: String,
    required: true
  }
});
module.exports = mongoose.model('Messages', messageSchema);