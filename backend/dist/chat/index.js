'use strict';

var _Message = require('./Message');

var _Message2 = _interopRequireDefault(_Message);

var _Conversation = require('./Conversation');

var _Conversation2 = _interopRequireDefault(_Conversation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var async = require("async");
var eachOf = require('async/eachOf');

module.exports = {
  createConversation: function createConversation(users, callback) {
    var conversation = new _Conversation2.default();
    conversation.generateConversationId(users, function (conversationIdDetails) {
      callback(conversationIdDetails);
    });
  },
  getConversationslastMessage: function getConversationslastMessage(conversationIds, callback) {
    var conversations = new Map();
    async.forEachOf(conversationIds, function (value, key, callback) {
      var conversation = new _Conversation2.default(value);
      conversation.getLastMessage(function (resp) {
        conversations[value] = resp;
        callback();
      });
    }, function (err) {
      if (err) console.error(err.message);
      callback(conversations);
    });
  },
  sendMessage: function sendMessage(theMessage, theSocket) {
    var message = new _Message2.default(theMessage);
    message.sendMessage(theSocket);
    // console.log(theMessage, "Socket!!!!")
  },

  getMessages: function getMessages(conversationId, callback) {
    var conversation = new _Conversation2.default(conversationId);
    conversation.getMessages(function (respond) {
      callback(respond);
    });
  }
};