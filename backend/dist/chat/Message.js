'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Echo = require('../utils/sockets');
var lib = require('../utils/misc-libraries');
var MessageModel = require('./MessageModel');
var uuid = require('../utils/misc-libraries').generateUuid;

var Message = function () {
    function Message(message) {
        _classCallCheck(this, Message);

        this._message = message;
    }

    _createClass(Message, [{
        key: 'sendMessage',
        value: function sendMessage(socket) {
            var _this = this;
            this._serverPunchingMessage();
            this._saveMessage(function (message) {
                if (message) {
                    // _this._message = message;
                    _this._notifyReceiver(socket);
                    _this._acknowledgeMessageSender(socket);
                } else _this._rejectMessageSender(socket);
            });
        }
    }, {
        key: '_checkValidMessage',
        value: function _checkValidMessage() {
            if (['message', 'from', 'to', 'conversationId', 'id'] in this._message) {
                // check if conversationId is valid
                // check if from is valid
                // check if to is valid
                return true;
            } else return false;
        }
    }, {
        key: '_serverPunchingMessage',
        value: function _serverPunchingMessage() {
            if (this._checkValidMessage) {
                console.log(this._message);
                this._message.time = new Date();
                this._message.status = 1;
            }
        }
    }, {
        key: '_saveMessage',
        value: function _saveMessage(callBack) {
            var message = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            var theMessage = message != null ? message : this._message;
            console.log("THE MESSAGE", theMessage);
            var messageDoc = new MessageModel(theMessage);
            delete messageDoc._id;
            console.log("messageDoc", messageDoc);
            messageDoc.save().then(function (doc) {
                console.log("DOC", doc);
                callBack(doc);
            }).catch(function (err) {
                console.log("Error saving message", err);
                callBack(false);
            });
        }
    }, {
        key: '_notifyReceiver',
        value: function _notifyReceiver(socket) {
            Echo.broadcast(socket, this._message.to, {
                property: 'receiveMessage',
                content: this._message
            });
        }
    }, {
        key: '_acknowledgeMessageSender',
        value: function _acknowledgeMessageSender(socket) {
            var echoMessage = Object.assign({}, this._message);
            console.log("echo message", echoMessage);
            delete echoMessage._id;
            echoMessage.id = uuid();
            var temp = echoMessage.from;
            echoMessage.from = echoMessage.to;
            echoMessage.to = temp;
            this._saveMessage(function (resp) {
                console.log("Echo....");
                Echo.emitToSender(socket, {
                    property: 'receiveMessage',
                    content: echoMessage
                });
            }, echoMessage);
        }
    }, {
        key: '_rejectMessageSender',
        value: function _rejectMessageSender(socket) {
            Echo.emitToSender(socket, {
                property: 'unsendMessage',
                content: this._message.id
            });
        }
    }]);

    return Message;
}();

exports.default = Message;