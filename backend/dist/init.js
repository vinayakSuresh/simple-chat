'use strict';

var cluster = require('cluster');
var express = require('express');
var bodyParser = require('body-parser');
var numCPUs = require('os').cpus().length;
var exp = express();
var http = require('http').Server(exp);
var io = require('socket.io')(http);
var Mongo = require('./utils/mongoInit');

module.exports = {
    app: exp,
    io: io
};

module.exports.app.use(bodyParser.json({
    limit: '500mb'
}));

(function () {
    if (cluster.isMaster) {
        console.log('Master ' + process.pid + ' is running');
        for (var i = 0; i < numCPUs; i++) {
            cluster.fork();
        }

        cluster.on('exit', function (worker, code, signal) {
            console.log('worker ' + worker.process.pid + ' died');
            cluster.fork();
        });
    } else {
        // Workers can share any TCP connection
        // In this case it is an HTTP server
        http.listen(4000, function () {
            console.log('listening to 4000');
        });

        module.exports.app.use(function (req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'Authorization,Content-Type');
            res.setHeader('Access-Control-Allow-Credentials', true);
            next();
        });

        console.log('Worker ' + process.pid + ' started');
    }
})();