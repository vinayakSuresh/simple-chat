'use strict';

var uuid = require('uuid');

module.exports = {
    generateUuid: function generateUuid() {
        return uuid.v4();
    }
};