"use strict";

var io = require('../init').io;

module.exports = {
    emitToRoom: function emitToRoom(socket, room, message) {
        socket.to(room).emit(message.property, message.content);
    },
    emitToRoomAndSender: function emitToRoomAndSender(room, message) {
        io.to(room).emit(message.property, message.content);
    },
    emitToSender: function emitToSender(socket, message) {
        console.log("Echo.... In Socket");
        socket.emit(message.property, message.content);
    },
    broadcast: function broadcast(socket, room, message) {
        socket.broadcast.to(room).emit(message.property, message.content);
    }
};