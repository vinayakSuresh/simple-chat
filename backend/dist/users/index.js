'use strict';

var _User = require('./User');

var _User2 = _interopRequireDefault(_User);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var async = require("async");
var eachOf = require('async/eachOf');
var UserModel = require('./UserModel');
var uuid = require('../utils/misc-libraries').generateUuid;

module.exports = {
    getConversationIds: function getConversationIds(userId, callback) {
        var user = new _User2.default(userId);
        user.getProperties(function (resp) {
            var response = resp ? resp.conversations : [];
            callback(response);
        });
    },
    createUser: function createUser(req, res) {
        var user = new _User2.default();
        user.create(req.body, function (resp) {
            if (res) {
                res.status(200).json({
                    success: true,
                    response: resp
                });
            } else return resp;
        });
    },
    createUsers: function createUsers() {
        var limit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 100;
        var callback = arguments[1];

        var users = [];
        for (var i = 1; i <= limit; i++) {
            users.push({
                firstName: 'User',
                lastName: i,
                status: 'Available',
                userId: uuid()
            });
        }
        UserModel.insertMany(users).then(function (doc) {
            callback(doc);
        }).catch(function (err) {
            console.error(err);
            callback(false);
        });
    },
    addConversation: function addConversation(conversationId, userIds, callback) {
        async.forEachOf(userIds, function (value, key, callback) {
            var user = new _User2.default(value);
            user.addConversation(conversationId, function (resp) {
                console.log("Updated");
                callback();
            });
        }, function (err) {
            if (err) console.error(err.message);
            callback();
        });
    },
    getUsers: function getUsers() {
        var search = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
        var callBack = arguments[1];

        var query = search ? [{
            $project: {
                status: 1,
                userId: 1,
                conversations: 1,
                fullName: {
                    $concat: ['$firstName', ' ', '$lastName']
                }
            }
        }, {
            $match: {
                fullName: {
                    $regex: search,
                    $options: 'i'
                }
            }
        }] : [{
            $project: {
                status: 1,
                userId: 1,
                conversations: 1,
                fullName: {
                    $concat: ['$firstName', ' ', '$lastName']
                }
            }
        }];

        UserModel.aggregate(query).then(function (doc) {
            console.log(doc);
            callBack(doc);
        }).catch(function (err) {
            console.error(err);
            callBack(false);
        });
    },
    getUser: function getUser(userId, callBack) {
        UserModel.findOne({
            userId: userId
        }).then(function (doc) {
            console.log(doc);
            callBack(doc);
        }).catch(function (err) {
            console.error(err);
            callBack(false);
        });
    }
};