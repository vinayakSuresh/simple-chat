'use strict';

var mongoose = require('mongoose');
var userSchema = new mongoose.Schema({
    userId: {
        type: String,
        unique: true,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String
    },
    status: {
        type: String
    },
    conversations: []
});
module.exports = mongoose.model('User', userSchema);