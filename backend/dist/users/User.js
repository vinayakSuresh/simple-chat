'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var uuid = require('../utils/misc-libraries').generateUuid;
var UserModel = require('./UserModel');

var User = function () {
    function User() {
        var userId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

        _classCallCheck(this, User);

        if (userId) this._userId = userId;else this._userId = uuid();
    }

    _createClass(User, [{
        key: 'create',
        value: function create(user, callBack) {
            user.userId = this._userId;
            var userDoc = new UserModel(user);
            userDoc.save().then(function (doc) {
                console.log("After save", doc);
                callBack(doc);
            }).catch(function (err) {
                console.log("Error creating user", err);
                callBack(false);
            });
        }
    }, {
        key: 'isValid',
        value: function isValid(callBack) {
            this.getProperties(function (resp) {
                if (resp) callBack(true);else callBack(false);
            });
        }
    }, {
        key: 'getProperties',
        value: function getProperties(callBack) {
            UserModel.findOne({
                userId: this._userId
            }).then(function (doc) {
                console.log(doc);
                callBack(doc);
            }).catch(function (err) {
                console.error(err);
                callBack(false);
            });
        }
    }, {
        key: 'addConversation',
        value: function addConversation(conversationId, callBack) {
            UserModel.findOneAndUpdate({
                'userId': this._userId
            }, {
                $addToSet: {
                    conversations: conversationId
                }
            }, {
                returnOriginal: false
            }).then(function (doc) {
                console.log(doc, ":::");
                callBack(doc);
            }).catch(function (err) {
                console.error(err);
                callBack(false);
            });
        }
    }, {
        key: 'delete',
        value: function _delete() {}
    }]);

    return User;
}();

exports.default = User;