import Message from './Message';
import Conversation from './Conversation';

const async = require("async");
const eachOf = require('async/eachOf');

module.exports = {
  createConversation: (users, callback) => {
    const conversation = new Conversation();
    conversation.generateConversationId(users, function (conversationIdDetails) {
      callback(conversationIdDetails);
    });
  },
  getConversationslastMessage: (conversationIds, callback) => {
    let conversations = new Map();
    async.forEachOf(conversationIds, function (value, key, callback) {
      let conversation = new Conversation(value);
      conversation.getLastMessage(function (resp) {
        conversations[value] = resp;
        callback();
      });
    }, function (err) {
      if (err) console.error(err.message);
      callback(conversations);
    });
  },
  sendMessage: (theMessage, theSocket) => {
    const message = new Message(theMessage);
    message.sendMessage(theSocket);
    // console.log(theMessage, "Socket!!!!")
  },

  getMessages: (conversationId, callback) => {
    const conversation = new Conversation(conversationId);
    conversation.getMessages(function(respond) {
      callback(respond);
    })
  }
}