const Echo = require('../utils/sockets');
const lib = require('../utils/misc-libraries');
const MessageModel = require('./MessageModel');
const uuid = require('../utils/misc-libraries').generateUuid;

export default class Message {
    constructor(message) {
        this._message = message;
    }

    sendMessage(socket) {
        let _this = this;
        this._serverPunchingMessage();
        this._saveMessage(function (message) {
            if (message) {
                // _this._message = message;
                _this._notifyReceiver(socket);
                _this._acknowledgeMessageSender(socket);
            } else _this._rejectMessageSender(socket);
        });
    }

    _checkValidMessage() {
        if (['message', 'from', 'to', 'conversationId', 'id'] in this._message) {
            // check if conversationId is valid
            // check if from is valid
            // check if to is valid
            return true;
        } else return false;
    }

    _serverPunchingMessage() {
        if (this._checkValidMessage) {
            console.log(this._message);
            this._message.time = new Date();
            this._message.status = 1;
        }
    }

    _saveMessage(callBack, message = null) {
        let theMessage = (message!=null) ? message : this._message;
        console.log("THE MESSAGE", theMessage);
        let messageDoc = new MessageModel(theMessage);
        delete messageDoc._id;
        console.log("messageDoc", messageDoc);
        messageDoc.save().then(doc => {
            console.log("DOC", doc);
            callBack(doc);
        }).catch(err => {
            console.log("Error saving message", err);
            callBack(false);
        });
    }

    _notifyReceiver(socket) {
        Echo.broadcast(socket, this._message.to, {
            property: 'receiveMessage',
            content: this._message
        });
    }

    _acknowledgeMessageSender(socket) {
        let echoMessage = Object.assign({}, this._message);
        console.log("echo message",echoMessage);
        delete echoMessage._id;
        echoMessage.id = uuid()
        const temp = echoMessage.from;
        echoMessage.from = echoMessage.to;
        echoMessage.to = temp;
        this._saveMessage(function(resp) {
            console.log("Echo....");
            Echo.emitToSender(socket, {
                property: 'receiveMessage',
                content: echoMessage
            });
        }, echoMessage);
    }

    _rejectMessageSender(socket) {
        Echo.emitToSender(socket, {
            property: 'unsendMessage',
            content: this._message.id
        });
    }
}