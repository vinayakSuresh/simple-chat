const uuid = require('../utils/misc-libraries').generateUuid;
const MessageModel = require('./MessageModel');

export default class Conversation {
    constructor(conversationId = null, users = []) {
        if (conversationId) this._conversationId = conversationId;
    }

    generateConversationId(users, callback) {
        let _this = this;
        let _new = false;

        function respond(resp) {
            if (resp.length) {
                _this._conversationId = resp[0].conversationId;
            } else {
                _this._conversationId = uuid();
                _new = true;
            }
            console.log(_this._conversationId);
            let response = {
                conversationId: _this._conversationId,
                new: _new
            };
            if (!_new) {
                response.messages = resp;
            }
            callback(response);
        }
        this.getConversationUsers(users, respond);
    }

    getConversationId() {
        return this._conversationId;
    }

    getLastMessage(callback) {
        this.getMessages(function(docs) {
            callback(docs[0] || null);
        }, 1, -1);
    }

    getMessages(callBack, limit = null, timeSort = 1) {
        MessageModel.find({
                conversationId: this._conversationId
            }).sort({
                time: timeSort
            }).limit(limit).then(doc => {
                callBack(doc);
            })
            .catch(err => {
                console.error(err);
                // callBack(false);
            });
    }
    
    getConversationUsers(users, callBack) {
        const query = {
            '$or': [{
                'from': users[0],
                'to': users[1]
            }, {
                'from': users[1],
                'to': users[0]
            }]
        };
        console.log(query);
        MessageModel.find(query).then(doc => {
                console.log(doc, "SOMETHING");
                callBack(doc);
            })
            .catch(err => {
                console.error(err);
                callBack(false);
            });
    }

    delete() {}
    archive() {}
}