const io = require('../init').io;

module.exports = {
    emitToRoom: function (socket, room, message) {
        socket.to(room).emit(message.property, message.content);
    },
    emitToRoomAndSender: function(room, message) {
        io.to(room).emit(message.property, message.content);
    },
    emitToSender: function (socket, message) {
        console.log("Echo.... In Socket");
        socket.emit(message.property, message.content);
    },
    broadcast: function (socket, room, message) {
        socket.broadcast.to(room).emit(message.property, message.content);
    }
}