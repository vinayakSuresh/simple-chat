var uuid = require('uuid');

module.exports = {
    generateUuid: () => {
        return uuid.v4();
    }
}