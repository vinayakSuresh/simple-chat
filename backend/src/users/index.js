import User from './User';
const async = require("async");
const eachOf = require('async/eachOf');
const UserModel = require('./UserModel');
const uuid = require('../utils/misc-libraries').generateUuid;

module.exports = {
    getConversationIds: (userId, callback) => {
        const user = new User(userId);
        user.getProperties(function (resp) {
            let response = (resp) ? resp.conversations : [];
            callback(response);
        });
    },
    createUser: (req, res) => {
        const user = new User();
        user.create(req.body, function (resp) {
            if (res) {
                res.status(200).json({
                    success: true,
                    response: resp
                });
            } else return resp
        });
    },
    createUsers: (limit = 100, callback) => {
        let users = [];
        for (let i = 1; i <= (limit); i++) {
            users.push({
                firstName: 'User',
                lastName: i,
                status: 'Available',
                userId: uuid()
            });
        }
        UserModel.insertMany(users)
            .then(doc => {
                callback(doc);
            })
            .catch(err => {
                console.error(err);
                callback(false);
            });
    },
    addConversation: (conversationId, userIds, callback) => {
        async.forEachOf(userIds, function (value, key, callback) {
            const user = new User(value);
            user.addConversation(conversationId, function (resp) {
                console.log("Updated");
                callback();
            });
        }, function (err) {
            if (err) console.error(err.message);
            callback();
        });
    },
    getUsers: (search = null, callBack) => {
        const query = (search) ? [{
                $project: {
                    status: 1,
                    userId: 1,
                    conversations: 1,
                    fullName: {
                        $concat: ['$firstName', ' ', '$lastName']
                    }
                }
            },
            {
                $match: {
                    fullName: {
                        $regex: search,
                        $options: 'i'
                    }
                }
            }
        ] : [{
            $project: {
                status: 1,
                userId: 1,
                conversations: 1,
                fullName: {
                    $concat: ['$firstName', ' ', '$lastName']
                }
            }
        }]

        UserModel.aggregate(query)
            .then(doc => {
                console.log(doc);
                callBack(doc);
            })
            .catch(err => {
                console.error(err);
                callBack(false);
            });
    },
    getUser: (userId, callBack) => {
        UserModel.findOne({
                userId: userId
            })
            .then(doc => {
                console.log(doc);
                callBack(doc);
            })
            .catch(err => {
                console.error(err);
                callBack(false);
            });
    }
}