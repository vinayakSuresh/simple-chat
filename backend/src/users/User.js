const uuid = require('../utils/misc-libraries').generateUuid;
const UserModel = require('./UserModel');

export default class User {
    constructor(userId = null) {
        if (userId) this._userId = userId;
        else this._userId = uuid();
    }

    create(user, callBack) {
        user.userId = this._userId;
        let userDoc = new UserModel(user);
        userDoc.save().then(doc => {
            console.log("After save", doc);
            callBack(doc);
        }).catch(err => {
            console.log("Error creating user", err);
            callBack(false);
        });
    }

    isValid(callBack) {
        this.getProperties(function (resp) {
            if (resp) callBack(true);
            else callBack(false);
        });
    }

    getProperties(callBack) {
        UserModel.findOne({
                userId: this._userId
            })
            .then(doc => {
                console.log(doc);
                callBack(doc);
            })
            .catch(err => {
                console.error(err);
                callBack(false);
            });
    }

    addConversation(conversationId, callBack) {
        UserModel.findOneAndUpdate({
                'userId': this._userId
            }, {
                $addToSet: {
                    conversations: conversationId
                }
            }, {
                returnOriginal: false
            }).then(doc => {
                console.log(doc, ":::");
                callBack(doc);
            })
            .catch(err => {
                console.error(err);
                callBack(false);
            });
    }

    delete() {}
}