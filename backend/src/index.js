const app = require('./init').app;
const io = require('./init').io;
const chat = require('./chat/index');
const users = require('./users/index');
const async = require("async");
const eachOf = require('async/eachOf');

app.get('/', (req, res) => {
    res.status(200).json({
        success: true,
        response: "Simple Chat"
    });
});

app.post('/createUser', (req, res) => {
    users.createUser(req, res);
});

app.post('/createUsers', (req, res) => {
        users.createUsers(req.body.limit, function(resp) {
            res.status(200).json({
                success: (resp) ? true : false,
                response: resp
            });
        });
});

app.post('/addConversation', (req, res) => {
    function respond(conversationIdDetails) {
        res.status(200).json({
            success: true,
            response: conversationIdDetails
        });
    }
    chat.createConversation(req.body.users, function(conversationIdDetails) {
        if (conversationIdDetails.new) {
            users.addConversation(conversationIdDetails.conversationId, req.body.users, function() {
                respond(conversationIdDetails);
            });
        } else respond(conversationIdDetails);
    });
});

app.post('/conversations', (req, res) => {
    function respond(conversations) {
        res.status(200).json({
            success: true,
            response: conversations
        });
    }
    users.getConversationIds(req.body.userId, function(resp) {
        chat.getConversationslastMessage(resp, respond);
    });
});

app.post('/messages', (req, res) => {
    function respond(messages) {
        res.status(200).json({
            success: true,
            response: messages
        });
    }
    chat.getMessages(req.body.conversationId, respond);
});

app.post('/users', (req, res) => {
    users.getUsers(req.body.search, function (resp) {
        res.status(200).json({
            success: (resp) ? true : false,
            response: (resp) ? resp : []
        });
    })
});

app.post('/user', (req, res) => {
    users.getUser(req.body.userId, function (resp) {
        res.status(200).json({
            success: (resp) ? true : false,
            response: (resp) ? resp : {}
        });
    })
});

app.get('/favicon.ico', (req, res) => res.status(204));

io.on('connection', function (socket) {
    console.log('Socket connected');

    socket.on('sendMessage', (message) => chat.sendMessage(message, socket));

    socket.on('user_connected', function (userCredentials) {
        socket.join(userCredentials.user_id);
        console.log(userCredentials.user_id, 'User Connected');
    });

    socket.on('user_disconnected', function (user_id) {});

    socket.on('user_start_typing', function (data) {
        socket.to(data.to).emit('user_start_typing', data);
    });

    socket.on('user_stop_typing', function (data) {
        socket.to(data.to).emit('user_stop_typing', data);
    });
});