const cluster = require('cluster');
const express = require('express');
const bodyParser = require('body-parser');
const numCPUs = require('os').cpus().length;
const exp = express();
const http = require('http').Server(exp);
const io = require('socket.io')(http);
const Mongo = require('./utils/mongoInit');

module.exports = {
    app: exp,
    io: io
};

module.exports.app.use(bodyParser.json({
    limit: '500mb'
}));

(function () {
    if (cluster.isMaster) {
        console.log(`Master ${process.pid} is running`);
        for (let i = 0; i < numCPUs; i++) {
            cluster.fork();
        }

        cluster.on('exit', (worker, code, signal) => {
            console.log(`worker ${worker.process.pid} died`);
            cluster.fork();
        });
    } else {
        // Workers can share any TCP connection
        // In this case it is an HTTP server
        http.listen(4000, function () {
            console.log('listening to 4000');
        });

        module.exports.app.use(function (req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'Authorization,Content-Type');
            res.setHeader('Access-Control-Allow-Credentials', true);
            next();
        });

        console.log(`Worker ${process.pid} started`);
    }
})();