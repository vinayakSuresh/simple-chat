## Simple Chat

> A simple two-coloumn chat application.

## DEMO

### Start Conversation
![Start Conversation](./Demo/startConversation.png) 

### Conversation
![Conversation](./Demo/conversation.png)

### Search People
![Search People](./Demo/searchPeople.png)