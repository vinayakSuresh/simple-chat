import axios from 'axios'
import io from 'socket.io-client'
import Vue from 'vue'

export const NODE_SERVER_URL = 'http://127.0.0.1:4000'
export const userId = '3101fa98-4c09-4cf2-91f4-87e4370c33f9'
Vue.prototype.$socket = io(NODE_SERVER_URL, {transports: ['websocket']})

export const chatRequests = axios.create({
  baseURL: NODE_SERVER_URL,
  headers: {
    Authorization: 'Bearer {token}',
    'Content-Type': 'application/json'
  }
});
