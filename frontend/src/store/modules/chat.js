import Vue from 'vue'
import {
  chatRequests
} from '../../config'
const uuidv4 = require('uuid/v4')

const state = {
  conversations: {},
  messages: [],
  users: {},
  currentUser: '4b0553d6-74cf-4f09-ba60-8907cf1feaae',
  otherUser: '',
  currentUserDoc: {},
  otherUserDoc: {},
  currentUserDetails: {},
  currentConversationId: ''
}

const getters = {
  _getter_conversations: state => state.conversations,
  _getter_messages: state => state.messages,
  _getter_users: state => state.users,
  _getter_currentUser: state => state.currentUser,
  _getter_otherUser: state => state.otherUser,
  _getter_currentUserDoc: state => state.currentUserDoc,
  _getter_otherUserDoc: state => state.otherUserDoc,
  _getter_currentConversation: state => state.currentConversationId
}

const mutations = {
  setConversations(state, value) {
    if (!value.reload) {
      Vue.set(state.conversations, value.conversationId, value);
    } else {
      Object.keys(value.messages).forEach(function (key) {
        Vue.set(state.conversations, key, value.messages[key]);
      })
    }
  },
  setUsers(state, value) {
    if (!value.reload) {
      Vue.set(state.users, value.userId, value);
    } else {
      Vue.set(state, 'users', {})
      value.users.forEach(function (user) {
        Vue.set(state.users, user.userId, user)
      })
    }
  },
  setMessages(state, value) {
    if (!value.reload) {
      state.messages.push(value.message)
    } else {
      state.messages = value.messages
    }
  },
  setConversationId(state, conversationId) {
    state.currentConversationId = conversationId
  },
  setOtherUserId(state, userId) {
    state.otherUser = userId
  },
  setUser(state, obj) {
    Vue.set(state, obj.user, obj.userDoc)
  }
}

const actions = {
  loadConversations({
    state,
    commit,
    dispatch
  }) {
    chatRequests.post('/conversations', {
      userId: state.currentUser
    }).then(function (resp) {
      console.log(resp.data.response, '::::')
      commit('setConversations', {
        messages: resp.data.response,
        reload: true
      })
    }).catch(function (error) {
      console.log(error)
    })
  },
  loadUsers({
    state,
    commit,
    dispatch
  }, search) {
    let searchQuery = (search) ? {
      search: search
    } : null
    chatRequests.post('/users', searchQuery).then(function (resp) {
      console.log(resp.data.response, '::::');
      commit('setUsers', {
        users: resp.data.response,
        reload: true
      })
    }).catch(function (error) {
      console.log(error)
    })
  },
  loadMessages({
    state,
    commit,
    dispatch
  }, userId) {
    chatRequests.post('/addConversation', {
      users: [userId, state.currentUser]
    }).then(function (resp) {
      console.log(resp.data.response, '::::')
      commit('setMessages', {
        messages: resp.data.response.messages || [],
        reload: true
      })
      commit('setConversationId', resp.data.response.conversationId)
      commit('setOtherUserId', userId)
      dispatch('loadOtherUserDoc')
    }).catch(function (error) {
      console.log(error)
    })
  },
  sendMessage({
    state,
    commit,
    dispatch
  }, sentMessage) {
    sentMessage.id = uuidv4()
    commit('setMessages', {
      message: sentMessage,
      reload: false
    })
    this._vm.$socket.emit('sendMessage', sentMessage)
  },
  loadCurrentUserDoc({
    state,
    commit,
    dispatch
  }) {
    chatRequests.post('/user', {
      userId: state.currentUser
    }).then(function (resp) {
      console.log("User Resp", resp);
      commit('setUser', {
        user: 'currentUserDoc',
        userDoc: resp.data.response
      })
    }).catch(function (error) {
      console.log(error);
    })
  },
  loadOtherUserDoc({
    state,
    commit,
    dispatch
  }) {
    chatRequests.post('/user', {
      userId: state.otherUser
    }).then(function (resp) {
      console.log("User Resp 1", resp);
      commit('setUser', {
        user: 'otherUserDoc',
        userDoc: resp.data.response
      })
    }).catch(function (error) {
      console.log(error)
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
